#!/bin/sh

CONFIGDIR="/etc/e2guardian"

#Load E2Guardian application configuration files if the directory is empty.
#Note: This copies all list files as well.
if [ "$(ls -A $CONFIGDIR)" ]; then
    echo "--> Pre-existing config files found in $CONFIGDIR"
else
    echo "--> Copying configuration files to $CONFIGDIR"
    cp -R /tmp/e2guardian-5.3.2/configs/. $CONFIGDIR/
    
    echo "--> Changing ownership of all config files to user e2guardian"
    chown -R e2guardian:users $CONFIGDIR /var/log/e2guardian
    find $CONFIGDIR -type d -exec chmod 770 {} \;
    find $CONFIGDIR -type f -exec chmod 660 {} \;
    
    echo "--> Removing superflous files"
    find $CONFIGDIR -type f -name .in -delete
    
    echo "--> Configuring e2guardian to work with external proxy, expecting port 3128"
    sed -i "s/#dockermode = off/dockermode = on/g" $CONFIGDIR/e2guardian.conf
    sed -i "s/#proxyip = 127.0.0.1/proxyip = 127.0.0.1/g" $CONFIGDIR/e2guardian.conf
    sed -i "s/#proxyport = 3128/proxyport = 3128/g" $CONFIGDIR/e2guardian.conf
fi

echo "Set proxy IP is $PROXYIP"
echo "Set proxy port is $PROXYPORT"
exec /usr/sbin/e2guardian >> /var/log/e2guardian/e2guardian_run.log 2>&1
