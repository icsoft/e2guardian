# E2Guardian

## Summary
E2Guardian is web content filter created by the authors at http://e2guardian.org.

This container is designed to be a minimal install of E2Guardian with access to an external proxy like Squid.

The currently recommended container for squid is icsoft/squid, instructions for running squid, with configuration is included in that container.

## Docker Image
### Container Location
The container can be downloaded from Docker Hub: https://hub.docker.com/r/icsoft/e2guardian

### Running the container
Please make sure Squid is running and you have tested it with a browser connection to an external web site before starting E2Guardian.
Running this container can be achieved by initially crating the directories for persistent storage for E2Guardian (change "~/docker" to your persistent storage directory otherwise it will be accessed in your roots home directory):

	mkdir -p ~/docker/e2guardian/config
	mkdir -p ~/docker/e2guardian/logs

Download and run E2Guardian:

	docker run --name e2guardian -d --restart=always \
		--publish 8080:8080 \
		--volume ~/docker/e2guardian/config:/etc/e2guardian \
		--volume ~/docker/e2guardian/logs:/var/log/e2guardian \
		icsoft/e2guardian


### E2Guardian Configuration
E2Guardian is defaultly configured to access Squid at the local loopback address (127.0.0.1:3128), you will need to point it at a specific Squid server address by editing ~/docker/e2guardian/config/e2guardian.conf and change "proxyip = 127.0.0.1" and "proxyport = 3128" to the correct Squid server IP address and port.

You can access all the E2Guardian web content filtering configuration at ~/docker/e2guardian/config/lists.

Make sure to add "detectportal.firefox.com" to "~/docker/e2guardian/config/lists/exceptionsitelist" for firefox access.

For documentation and configuration see DansGuardian Documentation Wiki.

#### E2Guardian Whitelisting
As E2Guardian uses storyboarding in V5, instead of adding "**" to ~/docker/e2guardian/config/lists/bannedsitelist, you need to edit ~/docker/e2guardian/config/examplef1.story and change it to read:

    function(checkblanketblock)
    if(true,,502) return setblock
    
and

    function(sslcheckblanketblock)
    if(true,,506) return setblock
    
Note that ALL comments have been removed as E2Guardian will try to interpret comments after a command.
Then add all accessable sites to exceptionsitelist and exceptionurllist in ~/docker/e2guardian/config/lists/ directory.

### Accessing the container
The container can be accessed in the normal docker way:

	docker exec -it e2guardian bash -l
	
### Source
The home of this container is at https://bitbucket.org/icsoft/e2guardian
