NAME = icsoft/e2guardian
IMAGEVERSION = v5.3.2_f
BUILDTIME = `date +%FT%T%z`
LISTENPORT = 8080
LOCALVOL = ${HOME}/docker

.PHONY: all build release clean run tag

all: build

build:
	@echo " --> Building docker container."
	docker build -t ${NAME}:${IMAGEVERSION} --build-arg LISTENPORT=${LISTENPORT} --rm ./

release:
	@echo " --> Re-tagging  docker image to latest, for upload to docker hub."
	docker tag ${NAME}:${IMAGEVERSION} ${NAME}:latest
	docker push ${NAME}

clean:
	@echo " --> Cleaning up test and dangling temporary files."
	rm -rf ${LOCALVOL}/e2guardian
	docker system prune
	-docker rm -f e2guardiantest
	-docker rmi ${NAME}:${IMAGEVERSION}
	-docker rmi ${NAME}:latest

run:
	@echo " --> Running E2Guardian container for testing. Needs a Squid running"
	@echo " --> Creating directories for persistance in home."
	mkdir -p ${LOCALVOL}/e2guardian/config
	mkdir -p ${LOCALVOL}/e2guardian/logs
	echo " --> starting container"
	docker run --name e2guardiantest -d \
	--publish ${LISTENPORT}:${LISTENPORT} \
	--volume ${LOCALVOL}/e2guardian/config:/etc/e2guardian \
	--volume ${LOCALVOL}/e2guardian/logs:/var/log/e2guardian \
	${NAME}:${IMAGEVERSION}
	CONTIP = `docker inspect --format='{{(index .NetworkSettings.Networks "bridge").IPAddress}}' e2guardiantest`
	echo " --> IP Address: ${CONTIP}"
	echo " --> Changing to insecure configuration for testing" 
	find ${LOCALVOL}/e2guardian -type d -exec chmod 755 {} \;
	find ${LOCALVOL}/e2guardian -type f -exec chmod 666 {} \;

tag:
	@echo " --> Tagging via git with version ${IMAGEVERSION}"
	git tag -a ${IMAGEVERSION} -m "release ${IMAGEVERSION}"
